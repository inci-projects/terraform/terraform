env_prefix = "demo"
avail_zone = ["ap-south-1a", "ap-south-1b"]
vpc_cidr = "20.0.0.0/16"
private_subnets = ["20.0.0.0/19", "20.0.32.0/19"]
public_subnets = ["20.0.64.0/19", "20.0.96.0/19"]
ec2_instance = "t3.micro"
ec2_type = "ON_DEMAND"
