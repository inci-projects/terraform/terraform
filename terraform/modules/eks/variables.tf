variable env_prefix {}
variable avail_zone {}
variable vpc_cidr {}
variable private_subnets {}
variable public_subnets {}
variable ec2_instance {}
variable ec2_type {}