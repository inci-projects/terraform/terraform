module "vpc" {
  source = "../vpc"

    avail_zone       = var.avail_zone
    private_subnets  = var.private_subnets
    public_subnets   = var.public_subnets
    env_prefix       = var.env_prefix
    vpc_cidr         = var.vpc_cidr
}

    #--IAM ROLE FOR EKS CLUSTER--#
resource "aws_iam_role" "eks-cluster" {
  name = "${var.env_prefix}-eks-cluster"
  tags = {
    Environment = "${var.env_prefix}-eks-cluster"
  }

  assume_role_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": [
                    "eks.amazonaws.com"
                ]
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
POLICY
}

    #--EKS CLUSTER POLICY ATTACHMENT--#
resource "aws_iam_role_policy_attachment" "eks-cluster-AmazonEKSClusterPolicy" {
  role       = aws_iam_role.eks-cluster.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}

    #--ROLE FOR NODE GROUP--#
resource "aws_iam_role" "nodes" {
  name = "${var.env_prefix}-eks-node-group"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

    #--NODE GROUP POLICY ATTACHMENT--#

resource "aws_iam_role_policy_attachment" "nodes-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.nodes.name
}

resource "aws_iam_role_policy_attachment" "nodes-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.nodes.name
}

resource "aws_iam_role_policy_attachment" "nodes-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.nodes.name
}
//----------------------------------------------------------------------------------------//

  #--EKS CLUSTER--#
resource "aws_eks_cluster" "eks-cluster" {
  name     = "${var.env_prefix}-eks-cluster"
  role_arn = aws_iam_role.eks-cluster.arn

  vpc_config {
    subnet_ids = [
      module.vpc.private_subnets[0],
      module.vpc.private_subnets[1],
      module.vpc.public_subnets[0],
      module.vpc.public_subnets[1]
    ]
  }

  # addon {
  #   addon_name = "coredns"
  #   subnet_ids = module.vpc.private_subnets
  # }

  # addon {
  #   addon_name = "kube-proxy"
  #   subnet_ids = module.vpc.private_subnets
  # }

  # addon {
  #   addon_name = "vpc-cni"
  #   subnet_ids = module.vpc.private_subnets
  # }
  
  depends_on = [
    aws_iam_role_policy_attachment.eks-cluster-AmazonEKSClusterPolicy
  ]
}

resource "aws_eks_addon" "core-dns" {
  cluster_name                = aws_eks_cluster.eks-cluster.name
  addon_name                  = "coredns"
  addon_version               = "v1.11.1-eksbuild.4"
  resolve_conflicts_on_update = "PRESERVE"
}

resource "aws_eks_addon" "kube_proxy" {
  cluster_name                = aws_eks_cluster.eks-cluster.name
  addon_name                  = "kube-proxy"
  addon_version               = "v1.29.0-eksbuild.1"
  resolve_conflicts_on_update = "PRESERVE"
}

resource "aws_eks_addon" "vpc_cni" {
  cluster_name                = aws_eks_cluster.eks-cluster.name
  addon_name                  = "vpc-cni"
  addon_version               = "v1.16.0-eksbuild.1"
  resolve_conflicts_on_update = "PRESERVE"
}

  #--NODE GROUP--#
resource "aws_eks_node_group" "node-group" {
  cluster_name    = aws_eks_cluster.eks-cluster.name
  node_group_name = "${var.env_prefix}-node-group"
  node_role_arn   = aws_iam_role.nodes.arn

  subnet_ids = [
    module.vpc.public_subnets[0],
    module.vpc.public_subnets[1]
  ]

  capacity_type  = var.ec2_type
  instance_types = [var.ec2_instance]

  scaling_config {
    desired_size = 2
    max_size     = 3
    min_size     = 1
  }

#  # update_config {
#  #   max_unavailable = 1
#  # }

  labels = {
    node = "${var.env_prefix}-kubenode"
  }

  # taint {
  #   key    = "team"
  #   value  = "devops"
  #   effect = "NO_SCHEDULE"
  # }

  # launch_template {
  #   name    = aws_launch_template.eks-with-disks.name
  #   version = aws_launch_template.eks-with-disks.latest_version
  # }

  depends_on = [
    aws_iam_role_policy_attachment.nodes-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.nodes-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.nodes-AmazonEC2ContainerRegistryReadOnly,
  ]
}

# launch template if required

# resource "aws_launch_template" "eks-with-disks" {
#   name = "eks-with-disks"

#   key_name = "local-provisioner"

#   block_device_mappings {
#     device_name = "/dev/xvdb"

#     ebs {
#       volume_size = 50
#       volume_type = "gp2"
#     }
#   }
# }