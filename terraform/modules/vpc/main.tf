        #--VPC--#
resource "aws_vpc" "vpc" {
    cidr_block           = var.vpc_cidr
    enable_dns_hostnames = true
    enable_dns_support   = true
    tags = {
        Name = "${var.env_prefix}-vpc"
        Environment = "${var.env_prefix}"
    }
}

        #--SUBNET--#

    #--INTERNET GATEWAY--#
resource "aws_internet_gateway" "ig" {
  vpc_id = "${aws_vpc.vpc.id}"
  tags = {
    Name        = "${var.env_prefix}-igw"
    Environment = "${var.env_prefix}"
  }
}

# /* Elastic IP for NAT */
# resource "aws_eip" "nat_eip" {
#   vpc        = true
#   depends_on = [aws_internet_gateway.ig]
# }

/* NAT */
# resource "aws_nat_gateway" "nat" {
#   allocation_id = "${aws_eip.nat_eip.id}"
#   subnet_id     = "${element(aws_subnet.public_subnet.*.id, 0)}"
#   depends_on    = [aws_internet_gateway.ig]
#   tags = {
#     Name        = "nat"
#     Environment = "${var.env_prefix}"
#   }
# }

resource "aws_subnet" "public-subnet" {
  count                   = 2
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.public_subnets[count.index]
  availability_zone       = var.avail_zone[count.index]
  map_public_ip_on_launch = true
  tags = {
    Name        = "${var.env_prefix}-public-subnet-${count.index + 1}"
    Environment = var.env_prefix
  }
}

    #--Private subnet--#
  resource "aws_subnet" "private-subnet" {
  count                   = 2
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.private_subnets[count.index]
  availability_zone       = var.avail_zone[count.index]
  map_public_ip_on_launch = false
  tags = {
    Name        = "${var.env_prefix}-private-subnet-${count.index + 1}"
    Environment = var.env_prefix
  }
}

    #--Routing table for private subnet--#
resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.vpc.id}"
  tags = {
    Name        = "${var.env_prefix}-private-route-table"
    Environment = "${var.env_prefix}"
  }
}

    #--Routing table for public subnet--#
resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.vpc.id}"
  tags = {
    Name        = "${var.env_prefix}-public-route-table"
    Environment = "${var.env_prefix}"
  }
}

resource "aws_route" "public_internet_gateway" {
  route_table_id         = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.ig.id}"
}

# resource "aws_route" "private_nat_gateway" {
#   route_table_id         = "${aws_route_table.private.id}"
#   destination_cidr_block = "0.0.0.0/0"
#   nat_gateway_id         = "${aws_nat_gateway.nat.id}"
# }

    #--Route table associations--#
resource "aws_route_table_association" "public" {
  count          = 2
  subnet_id      = aws_subnet.public-subnet[count.index].id
  route_table_id = aws_route_table.public.id
}

# resource "aws_route_table_association" "private" {
#   subnet_id      = "${element(aws_subnet.private_subnet.*.id, count.index)}"
#   route_table_id = "${aws_route_table.private.id}"
# }

    #--VPC's Security Group--#
resource "aws_security_group" "security-group" {
    name    = "${var.env_prefix}-sec-grp"
    vpc_id  = "${aws_vpc.vpc.id}"

    ingress {
        from_port = "0"
        to_port   = "0"
        protocol  = "-1"
        self      = "true"
    }

    egress {
        from_port = "0"
        to_port   = "0"
        protocol  = "-1"
        self      = "true"
    }
    tags = {
        Environment = "${var.env_prefix}"
    }
}

