terraform {
  required_version = ">= 0.12"
}

  //--VPC--//
module "vpc" {
  source = "./modules/vpc"

    avail_zone       = var.avail_zone
    private_subnets  = var.private_subnets
    public_subnets   = var.public_subnets
    env_prefix       = var.env_prefix
    vpc_cidr         = var.vpc_cidr
}

#     #--IAM ROLE FOR EKS CLUSTER--#
# resource "aws_iam_role" "eks-cluster" {
#   name = "${var.env_prefix}-eks-cluster"
#   tags = {
#     Environment = "${var.env_prefix}-eks-cluster"
#   }

#   assume_role_policy = <<POLICY
# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Effect": "Allow",
#             "Principal": {
#                 "Service": [
#                     "eks.amazonaws.com"
#                 ]
#             },
#             "Action": "sts:AssumeRole"
#         }
#     ]
# }
# POLICY
# }

#     #--EKS CLUSTER POLICY ATTACHMENT--#
# resource "aws_iam_role_policy_attachment" "eks-cluster-AmazonEKSClusterPolicy" {
#   role       = aws_iam_role.eks-cluster.name
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
# }

#     #--ROLE FOR NODE GROUP--#
# resource "aws_iam_role" "nodes" {
#   name = "${var.env_prefix}-eks-node-group"

#   assume_role_policy = jsonencode({
#     Statement = [{
#       Action = "sts:AssumeRole"
#       Effect = "Allow"
#       Principal = {
#         Service = "ec2.amazonaws.com"
#       }
#     }]
#     Version = "2012-10-17"
#   })
# }

#     #--NODE GROUP POLICY ATTACHMENT--#

# resource "aws_iam_role_policy_attachment" "nodes-AmazonEKSWorkerNodePolicy" {
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
#   role       = aws_iam_role.nodes.name
# }

# resource "aws_iam_role_policy_attachment" "nodes-AmazonEKS_CNI_Policy" {
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
#   role       = aws_iam_role.nodes.name
# }

# resource "aws_iam_role_policy_attachment" "nodes-AmazonEC2ContainerRegistryReadOnly" {
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
#   role       = aws_iam_role.nodes.name
# }

    //--EKS AND NODE GROUP--//
  module "eks" {
    source = "./modules/eks"

    avail_zone       = var.avail_zone
    private_subnets  = var.private_subnets
    public_subnets   = var.public_subnets
    vpc_cidr         = var.vpc_cidr
    env_prefix       = var.env_prefix
    ec2_type         = var.ec2_type
    ec2_instance     = var.ec2_instance
  }
