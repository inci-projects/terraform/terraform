terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/57562046/terraform/state/tfstate"
    username = "var.TF_VAR_USER"
    password = "var.TF_VAR_PASS"
  } 
}